# GNOME Circle Membership Guide

Thanks for your valuable contributions to the GNOME community. This document lists benefits granted by the Circle membership and how you can utilize them, as well as what's expected of you in terms of participation and effort.

## Benefits

### All Projects

- **Website listing:** Your project is listed on the [GNOME Circle](https://circle.gnome.org) website.
- **Foundation membership:** You and other significant contributors to your project qualify for [membership](https://www.gnome.org/foundation/membership) in the GNOME Foundation, which brings 
[many benefits](https://wiki.gnome.org/MembershipCommittee/MembershipBenefits) by itself. To become a member, submit a [membership application](https://gitlab.gnome.org/Teams/MembershipCommittee/-/issues/new?issuable_template=membership-application) where you mention your project's inclusion in Circle, as well as any other significant contributions you've made to the GNOME project.
- **Community translations:** Your project can enroll in [GNOME's translation system](https://l10n.gnome.org) and benefit from the high-quality work of our coordinated translation community. To get set up, open an [addition request](https://gitlab.gnome.org/Infrastructure/damned-lies/-/issues/new?issuable_template=Add%20module) and fill in the required information. You can use your project's presence in [apps.json](https://gitlab.gnome.org/Teams/Circle/-/blob/main/data/apps.json) or [libs.json](https://gitlab.gnome.org/Teams/Circle/-/blob/main/data/libs.json) as a reference.
- **Badges and boasting:** A membership in Circle is in many ways a quality seal, and you can show it off by using one of the available [badges](https://gitlab.gnome.org/Teams/Circle/-/tree/main/assets/button) on your project's website or in its source code repository. You're also free to promote it as "part of GNOME Circle".

### Apps

Apps get these additional benefits:

- **Apps for GNOME:** Your app gets its own page on the [Apps for GNOME](https://apps.gnome.org) website. You're encouraged to refer to this page in places where you want to provide details about your app, such as in the [homepage URL](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-url) field of your app's metadata or in social media posts.
- **Welcome to GNOME**: Your app gets its own page on the [Welcome to GNOME](https://welcome.gnome.org) website, with an automatically generated contribution guide based on the technologies used to develop it. You're encouraged to refer to this page in places like the [contribute URL](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-url) field of your app's metadata, unless contributing to the project requires specialized knowledge that you've documented elsewhere.
- **Software banner:** The [list of apps](https://gitlab.gnome.org/GNOME/gnome-software/-/blob/main/data/assets/org.gnome.Software.Featured.xml) to feature in the top banner of GNOME Software contains all Circle apps by default. It's updated now and then to reflect the current state of Circle.

## Expectations

As a maintainer of a GNOME Circle project, and by extension a recipient of the benefits above, there are also a couple of things that are expected of you. Taking the following into account should give you an enjoyable and frictionless experience, as well as preventing your project from deviating significantly from our standards. If the latter happens, an irregular review will be initiated so we can get to the bottom of what's happening together.

### Care for your project

When you initially submitted your project for inclusion, it had to pass a list of requirements. Maybe not all boxes were checked from the get-go, but during the review process, every requirement was eventually fulfilled. Going forward, every stable release of your project should keep fulfilling these criteria. This means that you ought to check compliance with the [HIG](https://developer.gnome.org/hig), perform [accessibility testing](https://developer.gnome.org/hig/guidelines/accessibility.html), keep the [project metadata](https://gitlab.gnome.org/World/apps-for-gnome/-/blob/main/METADATA.md) up-to-date, and ensure an overall great experience. If your project is an app, you can look for missing things in its metadata with the [app overview](https://sophie-h.pages.gitlab.gnome.org/app-overview/?annotations=true).

### Keep in touch

The GNOME community is full of people with different skills and interests. There are plenty of places to ask for help, collaborate, or just hang out, and you're encouraged to have a presence in some of these for both practical and social reasons. Here are some recommended ways to participate:

- Join the [GNOME Circle chat room](https://matrix.to/#/#circle:gnome.org) on Matrix for important announcements, questions, and coordination.
- Showcase big changes to your project on [This Week in GNOME](https://thisweek.gnome.org) by posting updates to its [Matrix chat room](https://matrix.to/#/#thisweek:gnome.org).
- When making significant changes to your app, ask the [GNOME Design community](https://matrix.to/#/#design:gnome.org) for guidance and feedback. This is extremely useful to avoid getting your app into a state where an irregular review has to be initiated.

Remember that these spaces, including the communication spaces of your project, are subject to the [GNOME Code of Conduct](https://conduct.gnome.org). If you ever witness a breach of these rules, please contact the [Code of Conduct Committee](https://handbook.gnome.org/foundation/committees/coc.html). 

### Recognize your limits

Developing and maintaining a free software project is no easy task, and at some point you might find yourself facing impossible time constraints or burnout. This is likely to make it hard to keep your project up to the standards of GNOME Circle. To ensure that your project is still useful for people and up-to-date, it's a very good idea to find co-maintainers who are willing to maintain the project alongside you, and, should you ever step down, take over the maintenance responsibility completely.

If that's not the case, however, and your project falls into an unmaintained state with nobody looking after it, you should notify the Circle Committee so it can gracefully exit from Circle.

---

Any questions regarding this document should be directed to the [GNOME Circle chat room](https://matrix.to/#/#circle:gnome.org) on Matrix. Changes can be suggested by opening an issue against this repository or directly with a merge request.
