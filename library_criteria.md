# GNOME Circle library criteria

 - [ ] Follows [GNOME coding guidelines](https://developer.gnome.org/documentation/guidelines/programming.html)
 - [ ] Targets glib-based applications
 - [ ] Targets interactive, end-user or graphical applications, or facilitates their development
 - [ ] Is used by at least one other project or app
 - [ ] Documentation provided (at least API reference docs)
 - [ ] Works as expected without serious issues (verify this by referring to the project's issue tracker and/or contacting users of the library)
 - [ ] No GNOME branding

#### Repository

  - [ ] CI builds of the library
  - [ ] Public issue tracker and code repository

#### Legal

  - [ ] Has an [OSI-approved license](https://opensource.org/licenses)
  - [ ] No contributor license agreement (CLA)
  - [ ] The software must work without installing proprietary software

#### Recommended

  - [ ] Build system is Meson, Automake, or CMake

## Additional criteria for GObject language bindings

In addition to the criteria above, a library that provides language bindings to
the GObject stack must fulfill the following requirements:

  - [ ] Supports composite templates
  - [ ] Supports subclassing (if the target language supports it)
  - [ ] Follows the [GObject Introspection guidelines](https://gi.readthedocs.io/en/latest/writingbindings/guidelines.html)
  - [ ] Confidence in the maintainers' ability to update the bindings *at least every GNOME release cycle*

  - [ ] Fully supports the latest major version of the following libraries:
    - [ ] GObject
    - [ ] GTK
    - [ ] GLib/GIO
    - [ ] libadwaita

  - [ ] The bindings must be mainly auto-generated, although hand-written parts are also allowed.
    - [ ] The above-mentioned library criteria must also apply to the binding generator.
