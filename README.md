# GNOME Circle

Welcome to GNOME Circle! GNOME Circle champions the great software that is available for the GNOME platform.

Member projects receive additional benefits and support from the GNOME Foundation and the GNOME Project, including Foundation membership, promotion and advertising, and translations from the community. For a comprehensive list of benefits, see the [Membership Guide](/membership_guide.md).

## Who can apply?

Apps and libraries can both apply for inclusion. To qualify they must have a good overall quality level and use an [OSI-approved license](https://opensource.org/licenses).

For detailed requirements please check our review criteria:

- [App Criteria](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppCriteria.md)
- [Library Criteria](library_criteria.md)

For questions, you can contact the GNOME Circle Committee via the [#circle:gnome.org Matrix channel](https://matrix.to/#/#circle:gnome.org).

## Code of Conduct

Circle projects are expected to follow [GNOME's Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct),
and to assist GNOME's Code of Conduct Committee if required.

If you are experiencing any problems, don't hesitate to reach out to the wider community, the Circle Committee, or the [Code of Conduct Committee](https://wiki.gnome.org/Foundation/CodeOfConduct/ReporterGuide).

## How to apply?

Project maintainers can submit their projects for inclusion by creating an issue with the respective issue template.

- [**Submit an app to GNOME Circle**](https://gitlab.gnome.org/Teams/Circle/-/issues/new?issuable_template=app_application)
- [**Submit a library to GNOME Circle**](https://gitlab.gnome.org/Teams/Circle/-/issues/new?issuable_template=library_application)

## Who runs GNOME Circle?

GNOME Circle is a GNOME Foundation initiative and is run by the [GNOME Circle Committee](https://handbook.gnome.org/foundation/committees/circle.html). If you're interested in joining the committee, please contact one of the existing members.
