# simple script to fetch apps details from Flathub
import json
import requests
import os
from pathlib import Path
import bs4


APPS_DATA_FILE = Path("./data/apps.json")
LIBS_DATA_FILE = Path("./data/libs.json")
ICONS_CACHE = Path("./assets/apps")
OUTPUT_DIR = Path("./output")

def fetch_app(app_id):
    uri = f"https://flathub.org/api/v1/apps/{app_id}"
    response = requests.get(uri)
    if response.ok:
        return response.json()
    print(f"Failed to fetch {app_id} details from Flathub")
    return None

def cache_icon(app):    
    icon = app["iconDesktopUrl"]
    response = requests.get(icon)
    if response.ok:
        icon_path = ICONS_CACHE / icon.split("/")[-1]
        icon_path.write_bytes(response.content)
        return icon_path
    return None


def generate_app_tag(soup, app, icon):
    # generate an html tag to be inserted for a specific app
    li = soup.new_tag("li")
    href = soup.new_tag("a", href=f"https://apps.gnome.org/app/{app['flatpakAppId']}")
    image = soup.new_tag("img", src=icon.as_posix().replace("./", ""), alt="")
    label = soup.new_tag("div")

    name = soup.new_tag("div")
    name['class'] = "name"
    name.string = app["name"]
    summary = soup.new_tag("div")
    summary['class'] = "summary"
    summary.string = app["summary"]
    label.append(name)
    label.append(summary)

    li.append(href)
    href.append(image)
    href.append(label)
    return li

def generate_lib_tag(soup, name, url):
    # generate an html tag to be inserted for a specific library
    li = soup.new_tag("li")
    href = soup.new_tag("a", href=url)
    href.string = name
    li.append(href)
    return li

def main():
    with open('./index.html') as fd:
        soup = bs4.BeautifulSoup(fd.read(),features="html.parser")

    if not ICONS_CACHE.exists():    
        ICONS_CACHE.mkdir()
    if not OUTPUT_DIR.exists():
        OUTPUT_DIR.mkdir()

    apps_list = soup.find(class_='applist')
    apps_tags = []
    with open(APPS_DATA_FILE) as fd:
        apps = json.load(fd)
        for app_info in apps:
            app_id = app_info['app_id']
            app = fetch_app(app_id)
            icon = cache_icon(app)

            tag = generate_app_tag(soup, app, icon)
            apps_tags.append((app['name'], tag))
    apps_tags.sort(key=lambda tag: tag[0]) # sort by name
    for (_, tag) in apps_tags:
        apps_list.append(tag)

    libs_list = soup.find(class_='textlist')
    libs_tags = []
    with open(LIBS_DATA_FILE) as fd:
        libs = json.load(fd)
        for lib in libs:
            tag = generate_lib_tag(soup, lib['name'], lib['url'])
            libs_tags.append((app['name'], tag))
    libs_tags.sort(key=lambda tag: tag[0]) # sort by name
    for (_, tag) in libs_tags:
        libs_list.append(tag)

    with open(OUTPUT_DIR / 'index.html', 'w') as fd:
        fd.write(str(soup.prettify()))

    
if __name__ == "__main__":
    main()
