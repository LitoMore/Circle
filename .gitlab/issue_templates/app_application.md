<!--

Thanks for your interest in GNOME Circle!

Before submitting your app, please try to make sure that the app fulfills
most of the "General App Criteria" and "Circle App Criteria" linked below.

Remaining uncertainties can be sorted out during the submission process or
you can contact us in the #circle:gnome.org Matrix channel.

- https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppCriteria.md

Please also make yourself familiar with what we expect of GNOME Circle project maintainers.

- https://gitlab.gnome.org/Teams/Circle/-/blob/HEAD/membership_guide.md#expectations

Issue title: New app: <app name>

-->

##### App information

  - App name:
  - Code repository page:
  - Flathub page:

##### Code of Conduct

GNOME Circle projects are expected to follow [GNOME's Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct).

<!--

Please have a look at GNOME's Code of Conduct and establish a consensus about
following GNOME's Code of Conduct within the app's project. Afterward, confirm
the following statement by activating the checkbox.

-->

- [ ] I confirm the project's willingness to follow the GNOME Code of Conduct and to assist the GNOME's Code of Conduct Committee if required.