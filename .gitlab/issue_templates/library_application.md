<!--

Thanks for your interest in GNOME Circle! For inclusion criteria, please see here:

  https://gitlab.gnome.org/Teams/Circle/-/blob/main/README.md#who-can-apply

Remaining uncertainties can be sorted out during the submission process or
you can contact us in the #circle:gnome.org Matrix channel.

To apply, please provide the following details:

-->

##### Library information

  - Library name:
  - Code repository page:

##### List of apps using this library



##### Code of Conduct

GNOME Circle projects are expected to follow [GNOME's Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct).

<!--

Please have a look at GNOME's Code of Conduct and establish a consensus about
following GNOME's Code of Conduct within the app's project. Afterward, confirm
the following statement by activating the checkbox.

-->

- [ ] I confirm the project's willingness to follow the GNOME Code of Conduct and to assist the GNOME's Code of Conduct Committee if required.